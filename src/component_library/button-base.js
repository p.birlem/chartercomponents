import * as COLORS from './colors.js';

export default `
  .button-base {
    box-sizing: border-box;
    position: relative;
    border: 1px solid ${COLORS.grey2.hex};
    background: ${COLORS.white.hex};
    box-shadow: 0 2px 4px 0 rgba(${
      COLORS.black.rgb
    }, 0.05), 0 2px 8px 0 rgba(${COLORS.grey2.rgb}, 0.4);
    color: ${COLORS.grey.hex};
    cursor: pointer;
    text-align:center;
    text-align: center;
margin: 0 auto;
  }

  .button-base:focus {
    border: 1px solid ${COLORS.green.hex};
    box-shadow: 0 2px 4px 0 rgba(${
      COLORS.grey2.rgb
    }, 0.4), 0 2px 8px 0 rgba(${
  COLORS.blue3.rgb
}, 0.05), inset 0 0 0 1px ${COLORS.green.hex};
  }

  .button-base.selected {
    border-color: ${COLORS.green.hex};
    background: rgba(${COLORS.green.rgb}, 0.4);
    box-shadow: 0 2px 8px 0 rgba(${COLORS.green.rgb}, 0.25);
    color: ${COLORS.green.hex};
  }

  .button-base.selected:before {
    content: '';
    text-align: center
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    box-shadow: 0 2px 8px 0 rgba(${COLORS.green.rgb}, 0.25);
  }

  .button-base.selected:hover,
  .button-base.selected:focus {
    box-shadow: 0 2px 8px 0 rgba(${COLORS.green.rgb}, 0.25);
  }

  .button-base:hover {
    border-color: ${COLORS.green.hex};
    color: ${COLORS.green.hex};
  }

  .button-base:active {
    border: 1px solid ${COLORS.green.hex};
    background: rgba(${COLORS.green.rgb}, 0.1);
    color: ${COLORS.green.hex};
    box-shadow: none;
  }

  .button-base:disabled {
    border-color: rgba(${COLORS.grey2.rgb}, 0.25);
    box-shadow: none;
    color: rgba(${COLORS.grey3.rgb}, 0.75);
    font-weight: normal;
    pointer-events: none;

  }

  .button-base:disabled .icon {
    color: ${COLORS.grey5.hex};
  }
`;
